import datetime
import genetic
import unittest
import statistics
import time

class Benchmark(object):
	"""docstring for Benchmark"""
	def run(function):
		timings = []
		for i in range(100):
			startTime = time.time()
			function()
			seconds = time.time() - startTime
			timings.append(seconds)
			mean = statistics.mean(timings)
			print("{0} {1:3.2f} {2:3.2f}".format(1+i, mean, statistics.stdv(timings,mean) if i > 1 else 0))
			
class GuessPasswordTests(unittest.TestCase):

	def Hello_World(self):
		target = "Hello World!"
		self.guess_password(target)
	def test_For_I_am_fearfully_and_wonderfully_made(self):
		target = "For I am fearfully and wonderfully made."
		self.guess_password(target)
	
	def test_benchmark(self):
		genetic.Benchmark.run(self.test_For_I_am_fearfully_and_wonderfully_made)

	def guess_password(self, target):
		geneset = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!."
		startTime = datetime.datetime.now()

		def fnGetFitness(genes):                  
			return self.get_fitness(genes, target)

		def fnDisplay(candidate):                     
			self.display(candidate, startTime)
		optimalFitness = len(target)
		best = genetic.get_best(fnGetFitness, len(target), optimalFitness, geneset, fnDisplay)
		self.assertEqual(best.Genes, target)
	def display(self, candidate, startTime):
		timeDiff = datetime.datetime.now() - startTime
		print("{0}\t{1}\t{2}".format(candidate.Genes, candidate.Fitness, str(timeDiff)))

	def get_fitness(self, genes, target):
		return sum(1 for expected, actual in zip(target, genes) if expected == actual)

		
if __name__ == '__main__':
	unittest.main()#test_Hello_World()