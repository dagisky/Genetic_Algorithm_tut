<h2>Genetic Algorithm</h2>

Every organism has a set of rules, a blueprint so to speak, describing how that organism is built up from the tiny building blocks of life. These rules are encoded in the genes of an organism, which in turn are connected together into long strings called chromosomes. Each gene represents a specific trait of the organism, like eye colour or hair colour, and has several different settings. For example, the settings for a hair colour gene may be blonde, black or auburn. These genes and their settings are usually referred to as an organism's genotype. The physical expression of the genotype - the organism itself - is called the phenotype.

When two organisms mate they share their genes. The resultant offspring may end up having half the genes from one parent and half from the other. This process is called recombination. Very occasionally a gene may be mutated. Normally this mutated gene will not affect the development of the phenotype but very occasionally it will be expressed in the organism as a completely new trait. The processes of natural selection - survival of the fittest - and gene mutation have very powerful roles to play in the evolution of an organism.

<table style="border:none">
<tr>
<td><img src="http://tvblogs.nationalgeographic.com/files/2014/03/tardigrade_swim.gif"></td>
</tr>
<tr>
<ul>
<li><b>Tardigrades</b>, also known as water bears</li>
<li>Can live in temperatures as low as -457 degrees and heat as    high as 357 degrees</li>
<li>The only known species that can live in vacuum</li>
<li>Can survive extreme radiation</li>
<li>Found everywhere in the world, from the highest mountains to the deepest oceans</li>
<li>Survived all the great 5 extinctions</li>
</ul>
<h4><span style="color:red">FREAK of NATURE-<span> OR THE PERFECT ORGANISM</h4>
</tr>
</table>


Life on earth has evolved to be as it is through the processes of natural selection, recombination and mutation.

**Genetic Algorithms** are a way of solving problems by mimicking the same processes Mother Nature uses. They use the same combination of selection, recombination and mutation to evolve a solution to a problem.

A fundamental aspect of solving problems using genetic algorithms is that they must provide feedback that helps the engine select the better of two guesses. That feedback is called the fitness,
Imagine you are given 10 chances to guess a number between 1 and 1000, with only right or wrong as feedback, you have no way to improve your guesses. If instead you receive higher or lower feedback indicating that the number is higher or lower than your guess, you can always find the number easily.

Now imagine multiplying the size of this problem so that instead of trying to find 1 number you are simultaneously trying to find a set of 100 numbers, all in the range 1 to 1000, your only receive back a fitness value indicating how closely that set of numbers matches the desired outcome. Your goal would be to maximize or minimize that fitness.

Genetic algorithms and genetic programming are very good at finding solutions to very large problems. They do it by taking millions of samples from the search space, making small changes, possibly recombining parts of the best solutions, comparing the resultant fitness against that of the current best solution, and keeping the better of the two 

<h4>References</h4>
<ul>
<li>Genetic Algorithm with Python by Cliton Sheppard</li>
<li>nationalgeographic.com</li>
</ul>
