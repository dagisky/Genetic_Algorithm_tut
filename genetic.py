import random
import statistics
import time

class Chromosome(object):
	"""docstring for Chromosome"""
	Genes = None
	Fitness = None

	def __init__(self,  genes, fitness):
		self.Genes = genes
		self.Fitness = fitness


class Benchmark(object):
	"""docstring for Benchmark"""
	def run(function):
		timings = []
		for i in range(100):
			startTime = time.time()
			function()
			seconds = time.time() - startTime
			timings.append(seconds)
			mean = statistics.mean(timings)
			print("{0} {1:3.2f} {2:3.2f}".format(1+i, mean, statistics.stdev(timings,mean) if i > 1 else 0))
		

def gen_parent(getfitness,lenght, geneSet):
	genes = []
	while len(genes) < lenght:
		sampleSize = min(lenght - len(genes), len(geneSet))
		genes.extend(random.sample(geneSet, sampleSize))
		genes = ''.join(genes)
		fitness = getfitness(genes)
		return Chromosome(genes,fitness)



def mutate(getfitness, parent, geneSet):
	index  = random.randrange(0, len(parent.Genes)) 
	# generate random index, which will be used for random mutation
	childGenes = list(parent.Genes)
	newGene, alternate = random.sample(geneSet, 2)
	childGenes[index] = alternate \
		if newGene == childGenes[index] \
		else newGene  
		#Mutate.. chk if the random gnerated character is similar to the existing one, if so change it
	genes = ''.join(childGenes)
	fitness = getfitness(genes)
	return Chromosome(genes, fitness)
	#Note: the childern are obtained throuh mutation, not sexual reproduction lol ;)


def get_best(get_fitness, targetLen, optimalFitness, geneSet, display):
	random.seed()
	bestParent = gen_parent(get_fitness, targetLen, geneSet)	
	display(bestParent)
	if bestParent.Fitness >= optimalFitness:
		return bestParent
	while True:
		child = mutate(get_fitness, bestParent, geneSet)
		
		if bestParent.Fitness >= child.Fitness:
			continue
		#if the existing best fitness is greater than that of the child fitness then try again
		display(child)
		if child.Fitness >= optimalFitness: 
			# check if the solution is discouvered chk fn=getfitness, it gives +1 for every correct word in the string
			return child 
			# solution discouvered, break loop
		bestParent = child

